# Serverless Gin Example

[Gin Go web framework](https://github.com/gin-gonic/gin) +
[Serverless framework](https://serverless.com/).
Using https://github.com/awslabs/aws-lambda-go-api-proxy to get up and running
with Gin in Lambda.

## Run it

```shell
# run the tests
make test

# compile the go code into bin/
make build

# deploy the service
sls deploy -v

# grab the "ServiceEndpoint"
ServiceEndpoint=$(sls info -v | grep ServiceEndpoint | cut -d ' ' -f 2)


# send HTTP requests to the endpoints

curl "${ServiceEndpoint}/ping"

curl "${ServiceEndpoint}/user/$(whoami)"

curl "${ServiceEndpoint}/user/$(whoami)/programming"

curl "${ServiceEndpoint}/welcome"

curl "${ServiceEndpoint}/welcome?firstname=Jane&lastname=Doe"

curl -X POST \
  -H content-type:multipart/form-data \
  -F message=formMessage \
  -F extraField=notUsed \
  "${ServiceEndpoint}/form_post"

curl -X POST \
  -H content-type:application/x-www-form-urlencoded \
  -d 'name=manu&message=this_is_great' \
  "${ServiceEndpoint}/post?id=1234"

# destroy the service
sls remove -v
```
